import React, {useState, useEffect} from 'react';
import user from '../assets/img/user.png';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const Profile = () => {
  const data = window.localStorage.getItem('profile')  != null ? JSON.parse(window.localStorage.getItem('profile')) : {email: '', mobile: ''} ;
  const [profile, setProfile] = useState({...data});
  const onChange = e => setProfile({...profile,  [e.target.name]: e.target.value})
  const {email, mobile} = profile;

  const setLocalData = (e) => {
    e.preventDefault();
    window.localStorage.setItem('profile', JSON.stringify(profile));
  }

  useEffect(()=> {
    setProfile({...profile})
  },[])


  return (
    <div>
      <h2>Profile</h2>
      <div className="user">
      <img src={user} height="200"  />
      </div>
      <div className="profileDetail">
         <Form onSubmit={setLocalData}>
          <Form.Group controlId="exampleForm.ControlInput1">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="name@example.com" name="email" value={email} onChange={onChange} />
          </Form.Group>
          <Form.Group controlId="exampleForm.ControlInput1">
            <Form.Label>Mobile</Form.Label>
            <Form.Control type="number" placeholder="8888888888" name="mobile" value={mobile} onChange={onChange} />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    </div>
  );
}

export default Profile;