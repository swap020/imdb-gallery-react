import React, {useState, useEffect} from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image'

const Detail = () => {
  const film = useParams().id;
  const [imgInfo, setImgInfo] = useState({})

  const header = {
  "x-rapidapi-host": "imdb-internet-movie-database-unofficial.p.rapidapi.com",
  "x-rapidapi-key": "", // Add your Key
  "useQueryString": true
  }

  useEffect(() => {
    axios.get(`https://imdb-internet-movie-database-unofficial.p.rapidapi.com/film/${film}`, {headers: header}).then(res =>{
    setImgInfo(res.data);
    }, err => console.log(err))
  }, [film, header]);

  return (
    <Container>
    <h2>{imgInfo.title}</h2>
      <Row>
        <Image src={imgInfo.poster} fluid />
      </Row>
      <Row>
      <Col>
        <h5>{imgInfo.plot ? 'Plot' :  null}</h5>
        <p>{imgInfo.plot}</p>

        {imgInfo.technical_specs ? imgInfo.cast.length > 0 ? <h5>Technical Specs</h5> : null :  null  }
        {
          imgInfo.technical_specs?.map(specs => 
        <ul>
          <li>{`${specs[0]} : ${specs[1]}`}</li>
        </ul>
        )}


        {imgInfo.cast ? imgInfo.cast.length > 0 ? <h5>Cast</h5> : null :  null  }
        {
          imgInfo.cast?.map(cast => 
        <ul>
          <li>{cast.actor}</li>
        </ul>
        )}
      </Col>
      </Row>
    </Container>
  );
}

export default Detail; 