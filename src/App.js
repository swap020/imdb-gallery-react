import React, { Fragment } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import Customer from './components/customer';
import Gallery from './components/gallery';
import Detail from './components/detail';
import Profile from './components/profile';
import Header from './components/includes/header';
import { createStore } from "redux";
import RootReducer from './reducer/RootReducer';
import { Provider } from "react-redux";

import 'bootstrap/dist/css/bootstrap.min.css';

export const Store = createStore(RootReducer);
function App() {
  return (
<Provider store={Store}>
<Router>
        <Fragment>
          <Header />
          <Switch>
              <Route exact path="/" component={Profile} />
              <Route exact path="/gallery" component={Gallery} />
              <Route exact path="/detail/:id" component={Detail} />
              <Route exact path="/customer" component={Customer} />
          </Switch>
        </Fragment>
    </Router>  
    </Provider>
    );
}

export default App;
