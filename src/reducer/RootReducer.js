
const initialState = {
  name: '', 
  email: '', 
  mobile: '', 
  address: ''
};

const RootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'UPDATE': {
      return {
        ...action.payload
      };
    }

    default: {
      return { ...state };
    }
  }
};

export default RootReducer;